call plug#begin('~/.config/nvim/bundle')

Plug 'ctrlpvim/ctrlp.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'terryma/vim-multiple-cursors'
Plug 'ervandew/supertab'
Plug 'Valloric/YouCompleteMe'
Plug 'SirVer/ultisnips'
Plug 'scrooloose/nerdcommenter'
Plug 'justinmk/vim-sneak'
Plug 'mtth/scratch.vim'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-repeat'
Plug 'vim-scripts/restore_view.vim'
Plug 'vim-scripts/YankRing.vim'
"Plug 'vim-scripts/ShowMarks'
"Plug 'mattn/webapi-vim'
Plug 'mattn/gist-vim'
"Plug 'christoomey/vim-tmux-navigator'

" language
Plug 'cakebaker/scss-syntax.vim'
Plug 'leafgarland/typescript-vim'
Plug 'pangloss/vim-javascript'
Plug 'tikhomirov/vim-glsl'
Plug 'JulesWang/css.vim'
Plug 'csscomb/vim-csscomb'
Plug 'wting/rust.vim'

" themes
Plug 'mhumeSF/one-dark.vim'
Plug 'Sclarki/neonwave.vim'

call plug#end()

" shell to use
set shell=/usr/bin/bash 

" Edit multiple unsaved files at the same time
set hidden

" enable nvim true colors
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" share yank buffer with clipboard
set clipboard+=unnamedplus

" leader
let mapleader=','

" Set the working dir from the opened file
set autochdir

syntax enable
set expandtab
set smarttab
set shiftwidth=2
set tabstop=2

" theme
set background=dark
colorscheme neonwave
" highlight current line
set cursorline

" toggle between relative and absolute line number
function! NumberToggle()
  if(&relativenumber == 1)
    set number
    set norelativenumber
  else
    set nonumber
    set relativenumber
  endif
endfunc

" relative line number by default
set relativenumber

" status line
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P

" ctrlp
let g:ctrlp_custom_ignore = 'node_modules\|.git\|build\|third-party\|third_party'
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:20,results:20'
let g:ctrlp_show_hidden = 1
nnoremap <silent> <leader>p :CtrlP<CR>
nnoremap <silent> <leader>b :CtrlPBuffer<CR>

" youcompleteme
set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0

" make youcompleteme works with utlisnips
" see https://medium.com/brigade-engineering/sharpen-your-vim-with-snippets-767b693886db#.tur27iggq
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:SuperTabCrMapping = 0
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
let g:ycm_key_list_select_completion = ['<C-j>', '<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-k>', '<C-p>', '<Up>']

" to trigger watchers refresh
set backupcopy=yes

" diff
set diffopt=filler,vertical
map <leader>du :diffupdate<CR>

" fugitive
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>gp :Gpull<space>
nnoremap <leader>gu :Gpush<space>

" general
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap ; :
nnoremap j gj
nnoremap k gk
imap kj <Esc>
imap jj <Esc>

" terminal
tnoremap <Esc> <C-\><C-n>
tnoremap kj <C-\><C-n>
tnoremap jj <C-\><C-n>

" tabs and panes
nnoremap <leader>t :tabnew .<CR>
nnoremap <leader>s :new .<CR>
nnoremap <leader>v :vnew .<CR>
nnoremap <leader>j <C-W>j
nnoremap <leader>k <C-W>k
nnoremap <leader>h <C-W>h
nnoremap <leader>l <C-W>l

" yankring
nnoremap <silent> <leader>yr :YRShow<CR>

" Sneak vim
let g:sneak#s_next = 1

"replace 'f' with 1-char Sneak
nmap f <Plug>Sneak_f
nmap F <Plug>Sneak_F
xmap f <Plug>Sneak_f
xmap F <Plug>Sneak_F
omap f <Plug>Sneak_f
omap F <Plug>Sneak_F

"replace 't' with 1-char Sneak
nmap t <Plug>Sneak_t
nmap T <Plug>Sneak_T
xmap t <Plug>Sneak_t
xmap T <Plug>Sneak_T
omap t <Plug>Sneak_t
omap T <Plug>Sneak_T

" Nerd commenter
map <leader>/ <leader>c<space><CR>

" show only user marks
let showmarks_include = "abcdefghijklmnopqrstuvwxyz"   
let g:showmarks_textlower="`\t"

" fold
map <leader>fb zfi{
map <leader>fp zfi(
