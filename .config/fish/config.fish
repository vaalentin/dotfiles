# load colors for termite
eval (dircolors -c ~/.dircolors | sed 's/>&\/dev\/null$//')

# enable vi mode
# fish_vi_mode

# remove greeting
set fish_greeting

# git prompt
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showupstream 'yes'
set __fish_git_prompt_color_branch magenta
set __fish_git_prompt_show_informative_status ‘yes’

# git prompt status chars
set __fish_git_prompt_char_dirtystate 'd'
set __fish_git_prompt_char_untrackedfiles 'u'
set __fish_git_prompt_char_stagedstate 's'
set __fish_git_prompt_char_stashstate 't'
set __fish_git_prompt_char_upstream_ahead 'a'
set __fish_git_prompt_char_upstream_behind 'b'
set __fish_git_prompt_color_upstream_ahead blue
set __fish_git_prompt_color_upstream_behind red

# lefr prompt
function fish_prompt
  set -l home_escaped (echo -n $HOME | sed 's/\//\\\\\//g')
  set -l pwd (echo -n $PWD | sed "s/^$home_escaped/~/" | sed 's/ /%20/g')
  set -l prompt_symbol ''
  switch $USER
     case root toor; set prompt_symbol '#'
     case '*';  set prompt_symbol '$'
  end
  printf "%s@%s %s%s%s%s " $USER (hostname -s) (set_color blue) (prompt_pwd) (set_color normal) $prompt_symbol
end

# right prompt
function fish_right_prompt
  set_color normal
  printf "%s" (__fish_git_prompt)
end

# path
set PATH ~/.npm-packages/bin $PATH

set PG_OF_PATH ~/Repos/of
set PATH ~/Repos/of/apps/projectGenerator/commandLine/bin $PATH

# abbreviations

# directories
abbr repos "cd /data/repos"

# git
abbr gs 'git status'
abbr ga 'git add'
abbr gb 'git branch'
abbr gm 'git merge'
abbr gc 'git commit'
abbr gd 'git diff'
abbr go 'git checkout'
abbr gp 'git pull'
abbr gu 'git push'
abbr gh 'git history'

# others
abbr rw 'connmanctl disable wifi; connmanctl enable wifi'
abbr rx 'xmodmap ~/.Xmodmap'

# git utils
function get_git_formatted_date
  date '+%a %b %d %I:%M %Y %z'
end

function set_git_commit_date
  export GIT_AUTHOR_DATE="$argv"
  export GIT_COMMIT_DATE="$argv"
end

function reset_git_commit_date
  set_git_commit_date
end
