#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias vim='nvim'

# load colors for termite
eval $(dircolors ~/.dircolors)

# export PS1="\u@\h \w\\$ "

export VISUAL=nvim
export EDITOR=nvim

export PATH="$PATH:$HOME/.npm-packages/bin"

